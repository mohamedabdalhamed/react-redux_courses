import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css'
import '@popperjs/core'; // Edit here
import 'bootstrap/dist/js/bootstrap'
import 'jquery/dist/jquery'
import 'popper.js/dist/popper'
import '@fortawesome/fontawesome-free/css/all.css'
import './App.css'
import {Switch,Route} from "react-router-dom";
import Home from './Components/Home/Home';
import Navbar from './Components/Navbar/Navbar';
import Login from './Components/auth/Login/Login';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';
import RoutePravites from "./Components/auth/RoutePravites";
import Profile from "./Components/Profile/Profile";
import Register from "./Components/auth/Register/Register";
import DetailsCourse from "./Components/Home/DetailsCourse/DetailsCourse";
import myCourse from './Components/Course/MyCourse';
import Addcourse from "./Components/Course/Addcourse";
import All from './Components/Course/AllCourse/All';
import AllLikes from './Components/Home/AllLikes';
import Admin from './Components/Admin/Admin';
import Catgorey from './Components/Admin/Catgorey';
import Subgate from './Components/Admin/Subgate';
import ManageCouerse from './Components/Admin/ManageCouerse';
import RotutePravitesAdmin from './Components/auth/RotutePravitesAdmin.jsx';
import axios from 'axios';
import jwt_decode from "jwt-decode";
import Rateandfollow from './Components/Profile/Rateandfollow';
import Footer from './Components/Footer';

class App extends Component {
  state={id:''}

    getid=async()=>{
      var token = localStorage.getItem('token')
      var decoded = jwt_decode(token);

       let axiosConfig = {
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+token 
                }
          };
        let {data}=await axios.get("http://localhost:8001/api/roles/"+decoded.sub,axiosConfig)
        if(data.message==='success')
        {
            this.setState({id:data.type});
            console.log(this.state.id)
    
        }
    }
     componentDidMount(){
 
      this.getid()
     
    }


  render() {
    return (


<React.Fragment>
<Navbar   />

<ToastContainer
/>
      <Switch>
<RoutePravites  path="/home" component={Home} />
<RotutePravitesAdmin  path="/Admin" component={Admin} Admins={this.state.id} />
<RotutePravitesAdmin path="/Catgorey" component={Catgorey}  Admins={this.state.id} />
<RotutePravitesAdmin  path="/section" component={Subgate}  Admins={this.state.id}  />
<RotutePravitesAdmin   path="/Courses" component={ManageCouerse}  Admins={this.state.id}  />

<RoutePravites  path="/login" component={Login} />
<RoutePravites  path="/Profile" component={Profile} />
<RoutePravites  path="/Rateandfollow/:id" component={Rateandfollow} />

<RoutePravites  path="/register" component={Register} />
<RoutePravites  path="/detailsCourse/:id" component={DetailsCourse} />

<RoutePravites  path="/Addcourse/:id" component={Addcourse} />

<RoutePravites  path="/all/" component={All} />

<RoutePravites  path="/like" component={AllLikes} />

<RoutePravites  path="/myCourse/:id" component={myCourse} />

      </Switch>
      <Footer />
     </React.Fragment>
      
    );
  }
}

export default App;
