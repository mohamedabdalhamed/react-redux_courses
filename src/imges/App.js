import React from "react";

import './App.css';
import 'bootstrap/dist/css/bootstrap.css'
import 'jquery/dist/jquery.min.js'
import 'popper.js/dist/umd/popper.js'
import 'bootstrap/dist/js/bootstrap.bundle'
import {Switch,Route} from "react-router-dom";
import Home from './Components/Home/Home';
import Navbar from './Components/Navbar/Navbar';
import Login from './Components/auth/Login/Login';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';
import RoutePravites from "./Components/auth/RoutePravites";
import Upload from "./Components/Home/Upload/Upload";
import Profile from "./Components/Profile/Profile";
import Register from "./Components/auth/Register/Register";
import DetailsCourse from "./Components/Home/DetailsCourse/DetailsCourse";
import myCourse from './Components/Course/MyCourse';
import Addcourse from "./Components/Course/Addcourse";

export default function App() {
  return (
<React.Fragment>
<ToastContainer
/>
<Navbar   />
      <Switch>
<RoutePravites  path="/home" component={Home} />
<RoutePravites  path="/login" component={Login} />
{/* <Route path="/upload" component={Upload}/> */}
<Route path="/profile" component={Profile}/>
<RoutePravites  path="/register" component={Register} />
<RoutePravites  path="/detailsCourse/:id" component={DetailsCourse} />

<RoutePravites  path="/Addcourse/:id" component={Addcourse} />
<RoutePravites  path="/myCourse/:id" component={myCourse} />

      </Switch>
    </React.Fragment>
  );
}