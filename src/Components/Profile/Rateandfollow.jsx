import React,{useEffect, useState} from 'react'
import image from '../../imges/emojis/emoji-1.png'
import image2 from '../../imges/emojis/emoji-2.png'
import image3 from '../../imges/emojis/emoji-3.png'
import image4 from '../../imges/emojis/emoji-4.png'
import image5 from '../../imges/emojis/emoji-5.png'
import { useParams } from 'react-router-dom';

import './Rate.css'
import {connect} from 'react-redux';
import { getuserfun } from './../redux/actions';
import { axios } from 'axios';
import  jwt_decode  from 'jwt-decode';
function Rateandfollow(props) {
    const[rate,setrate]=useState({rate:'',id:''})
   const ongetreat=async(e)=>{
    let token=localStorage.getItem('token')
    var decoded = jwt_decode(token);
    
 
setrate({rate:e.currentTarget.value,id:decoded.sub})
console.log(rate)
e.preventDefault();
let axiosConfig = {
    headers: {
        'Accept': 'application/json',
        'Authorization':'Bearer '+token 
        }
  };
let {data}=await axios.get("http://localhost:8001/api/Rate/"+props.userse.id,rate,axiosConfig)
if(data.message==='success')
{

}

    }
  
    let {id}=useParams();
          
    useEffect(()=>{
        props.getuserfun(id)
        console.log(props)
    },[props.getuserfun(id)])
  return (
    <div className='container'>
            <script src="https://kit.fontawesome.com/a076d05399.js"></script>
<div className='d-flex'>
   <div className="wrapper">
               <input type="radio" name="rate" onChange={ongetreat} value={1} id="star-1"/>
    <input type="radio" name="rate" onChange={ongetreat} value={2}id="star-2"/>
    <input type="radio" name="rate" onChange={ongetreat}value={3}id="star-3"/>
    <input type="radio" name="rate" onChange={ongetreat}value={4} id="star-4"/>
    <input type="radio" name="rate" onChange={ongetreat}value={5} id="star-5"/>

    <div className="content">
      <div className="outer">
        <div className="emojis">
          <li className="slideImg"><img src={image} alt=""/></li> 
          <li><img src={image2} alt=""/></li> 
          <li><img src={image3} alt=""/></li> 
          <li><img src={image4} alt=""/></li> 
          <li><img src={image5} alt=""/></li> 
        </div>
      </div>
  

      <div className="stars">
        <label htmlFor="star-1" className="star-1 fas fa-star"></label>
        <label htmlFor="star-2" className="star-2 fas fa-star"></label>
        <label htmlFor="star-3" className="star-3 fas fa-star"></label>
        <label htmlFor="star-4" className="star-4 fas fa-star"></label>
        <label htmlFor="star-5" className="star-5 fas fa-star"></label>
      </div>
      <div className="footer">
      <span className="text"></span>
    </div>
    </div>
   
  </div>
  <div >
    
    <h2>{props.userse.name}</h2>

    <button className="btn btn-primary btn-sm follow mt-5 text-center">Follow</button>


        </div>
        
  </div>
     
    </div>
  )
}

function mapStateToProps(state){
    return {
      userse:state.users
      
    }
  }
  
export default connect(mapStateToProps,{getuserfun})(Rateandfollow)