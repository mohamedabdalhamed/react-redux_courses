
import React,{useEffect,useState} from 'react'
import './profile.css'
import imges from '../../imges/2.png'
import jwt_decode from "jwt-decode";
import axios from 'axios'
import { getuserfun } from './../redux/actions';
import { connect } from 'react-redux';
import { toast } from 'react-toastify';
import $ from 'jquery';

function Profile(props) {
    const[file,setfiles]=useState()
    const[id,setid]=useState()
    let token=localStorage.getItem('token')
    var decoded = jwt_decode(token);
    
 
    const getfiles=(e)=>{ 
          setfiles(e.currentTarget.files[0])
    }
    const submit=async(e)=>{
        setid()

        e.preventDefault()

        let axiosConfig = {
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+token 
                }
          };
          let format=new FormData()
          format.append('file',file)

          let {data}=await axios.post('http://localhost:8001/api/updateuser/'+decoded.sub,format,axiosConfig)
        if(data.message==="success"){
            props.getuserfun(decoded.sub);
            toast.info('success',{ theme: "light" })
            $('.modal-backdrop').hide()  
            $('#exampleModal').hide() 
        }
        

    }
    useEffect(()=>{
        if(props.userse.image===undefined){

        }else{
            props.getuserfun(decoded.sub);
        }
        props.getuserfun(decoded.sub);
        
    },[decoded.sub])
  return (
    <div className='profiles mt-5'>
    <div className="container d-flex justify-content-center align-items-center  ">
<div className="card">
    
    <div className="upper"> <img src="https://i.imgur.com/Qtrsrk5.jpg" className="img-fluid"/> </div>
    <div className="user text-center">

        <div className="profile"> 
        {props.userse.image ?   <img src={`http://localhost:8001/uploads/${props.userse.image}`} className="rounded-circle" width="80"/>
  :         <img src={imges} className="rounded-circle" width="80"/>
}
        
         </div>

    </div>
    <div className="mt-5 text-center">
    
    <i className="fas fa-edit fa-2x mt-4 cursors" data-bs-toggle="modal" data-bs-target="#exampleModal"></i>

        <h4 className="mb-0"></h4> {props.users.type}<span className="text-muted d-block mb-2  "></span> {props.userse.name} 
    <br/>         
     {props.userse.id!==id ? 
''        
        :
        <button className="btn btn-primary btn-sm follow">Follow</button>
        
        } 
        <div className="d-flex justify-content-between align-items-center mt-4 px-4">
            <div className="stats">
                <h6 className="mb-0">Followers</h6> <span>8,797</span>
            </div>
            <div className="stats">
                <h6 className="mb-0">Projects</h6> <span>{props.users.countcouese}</span>
            </div>
            <div className="stats">
                <h6 className="mb-0">Ranks</h6> <span>129</span>
            </div>
        </div>
    </div>
</div>
</div>
<div className="modal fade" id="exampleModal"  aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form onSubmit={submit}>
  <div className="modal-dialog">
    <div className="modal-content">
      <div className="modal-header">
        <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div className="modal-body">
           
                 <input placeholder="Type Title" name="file"  multiple onChange={getfiles}  className="form-control mt-4" type="file"  />
      </div>
      <div className="modal-footer">
        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" className="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
  </form>

</div>
</div>
  )
}
function map(state){
   return {users:state.getusers,
    userse:state.users

}
}
export default connect(map,{getuserfun})(Profile)
