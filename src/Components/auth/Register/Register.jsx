import React, { Component } from 'react'
import axios from 'axios';
import {  toast } from 'react-toastify';

export default class Register extends Component {
    state={name:'',
           email:'',
           password:'',
           errorsrig:[]           
}
getvalues=(e)=>{
        let state={...this.state}
    state[e.currentTarget.name]=e.currentTarget.value;
      this.setState(state)
      console.log(this.state)
}
sendinformation=async (e)=>{
    e.preventDefault( );
// let result=await axios.post()
// if(data.message==='valid')
// {
// // localStorage.setItem("token",data.token) 
// // toast("U Register")
// // this.props.history.replace("/home")
// console.log(data.Errors)
// }

// else{
// toast(data.message)
// }

//  if(result.data.code == 401)
// {
//   console.log("Sadsasd")

//   alert(result.data.success)
// }
await axios.post("http://localhost:8001/api/register", {...this.state})
    .then(response =>{
      localStorage.setItem("token",response.data.token) 
      this.props.actionsnews();
       toast("U Register")
      this.props.history.replace("/home")}
      )
    .catch(error => {
     let e= JSON.parse(error.request.response)
      this.setState({
        errorsrig:e
      })
    });


} 
    render() {
    return (
      <div className='login'>
      <div className="container my-3  d-flex justify-content-center">
          <form onSubmit={this.sendinformation} className="form mt-3">
          <h3 className='text-center'>Register</h3>

          <div className="mb-3">
        <label className="form-label text-dark"  >name</label>
        <input type="text" className="form-control" name='name'  onChange={this.getvalues}/>
        <div id="emailHelp" className="form-text text-white">
          {this.state.errorsrig.name ? this.state.errorsrig.name: ''}
        </div>
      </div>

      <div className="mb-3">
        <label htmlFor="exampleInputEmail1" className="form-label text-dark">Email address</label>
        <input type="email" className="form-control" id="exampleInputEmail1" name='email' onChange={this.getvalues} aria-describedby="emailHelp"/>
        <div id="emailHelp" className="form-text text-white">
        {this.state.errorsrig.name ? this.state.errorsrig.email: ''}

          </div>
      </div>
      <div className="mb-3">
        <label htmlFor="exampleInputPassword1" className="form-label">Password</label>
        <input type="password" className="form-control" id="exampleInputPassword1"onChange={this.getvalues} name='password'/>
        <div id="emailHelp" className="form-text text-white">
        {this.state.errorsrig.password ? this.state.errorsrig.password.map((item,index) =>(
        <div>
          {item} 
        </div>
        ) ) : ''}
          </div>

      </div>
      <div className="mb-3 form-check">
        <input type="checkbox" className="form-check-input" id="exampleCheck1"/>
        <label className="form-check-label" htmlFor="exampleCheck1">Check me out</label>
      </div>
      <button type="submit" className="btn btn-primary">Submit</button>
    </form></div>
    </div>
    )
  }
}
