import React from 'react'
import {Redirect,Route} from "react-router-dom";
import Login from './Login/Login';
import jwt_decode from "jwt-decode";
import Register from './Register/Register';

export default function RoutePravites(props) {
    var token = localStorage.getItem('token')
     
try{
    var decoded = jwt_decode(token);
     
}catch(ex) {
    if(props.path ==="/login"){
        return  <Route path={'/login'} component={Login} />

    }else if(props.path ==="/register"){
    
    return  <Route path={'/register'} component={Register} />
}   else{
            return  <Redirect  to="/login" />

}

}

if(props.path ==="/login"  ){

    return  <Redirect  to="/home" />
}else if(props.path ==="/register"){
    return  <Redirect  to="/home" />

}else{
return <Route { ...props}  />
}
}
