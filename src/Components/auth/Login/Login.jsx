import React, { Component } from 'react'
import axios from 'axios';
import {  toast } from 'react-toastify';
import {connect} from 'react-redux';
import { actionsnews } from  './../../redux/actions' ;
 class Login extends Component {

    state={
      email :'',
      password:'',
      errorMessage:'',
      singe:false
    }
  getvalues=(e)=>{
    console.log(this.props)
    let state={...this.state}
    state[e.currentTarget.name]=e.currentTarget.value;
      this.setState(state )
      console.log(this.state)
      
    }  
    senderinformation=async (e)=>{
        e.preventDefault( );
this.setState({singe:true})
 
        await axios.post("http://localhost:8001/api/login", {...this.state})
  .then(response => { 
     localStorage.setItem("token",response.data.token) 
     this.props.actionsnews();
    toast(response.message)
    this.props.history.replace("/home")
    
  })
  .catch(response => {
    toast("Paaword or email incorrect ")

    this.setState({ errorMessage: "Paaword or email incorrect" ,singe:false});
});
  } 


    render() {
        return (
          <div className='logins'>
            <div  className="container my-3 d-flex justify-content-center ">
             <form onSubmit={this.senderinformation} className="form mt-5">
                  <h2 className='text-center'>Login</h2>
  <div className=" mb-3">
    <label htmlFor="exampleInputEmail1" className="form-label text-dark">Email address</label>
    <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email" onChange={this.getvalues} />
    <div id="emailHelp" className="form-text text-dark">{this.state.errorMessage}</div>
  </div>
  <div className="mb-3">
    <label htmlFor="exampleInputPassword1" className="form-label text-dark">Password</label>
    <input type="password" className="form-control" id="exampleInputPassword1" name="password"  onChange={this.getvalues} />
  </div>
  <div className="mb-3 form-check">
    <input type="checkbox" className="form-check-input" id="exampleCheck1"/>
    <label className="form-check-label" htmlFor="exampleCheck1">Check me out</label>
  </div>
  <button type="submit" className="btn btn-primary">{this.state.singe ? 'wait....' : 'Submit'}</button>
</form>
            </div>
            </div>
        )
    }

}
function mapStateToProps(state){
  return {home:state.storedata}
}

export default connect(mapStateToProps,{actionsnews})(Login);