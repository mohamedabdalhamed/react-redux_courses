import axios from "axios";
import jwt_decode from "jwt-decode";

export  function actionsnews() {
 return async(dispatch)=>{
     let token=localStorage.getItem('token');
     if(token){
      let result=await axios.get('http://localhost:8001/api/user', {
        headers: {
        'Accept': 'application/json',
        'Authorization':'Bearer '+token 
        }
      }  )

dispatch({type:"apis",payload:result})
     }
 }
}


export  function course(id) {
  return async(dispatch)=>{
      let token=localStorage.getItem('token');
  
      const result=await axios.get("http://localhost:8001/api/course/"+id,{
        headers: {
        'Accept': 'application/json',
        'Authorization':'Bearer '+token 
        }
      })
    

  dispatch({type:"course",payload:result})


      
  }
 }

 export function getuserfun (id){

   return async(dispatch)=>{

    let token=localStorage.getItem('token')
    let axiosConfig = {
        headers: {
            'Accept': 'application/json',
            'Authorization':'Bearer '+token 
            }
      };
  
    let {data}=await axios.get('http://localhost:8001/api/getusers/'+id,axiosConfig)
    if(data.message==="success"){
      dispatch({type:"getusertype",payload:data})
      console.log(data)

    }
   }
  


}
 

 export function getsubcategoe(id){
  return async(dispatch)=>{

    var token = localStorage.getItem('token')
    const {data}=await axios.get("http://localhost:8001/api/showcosw/"+id,{
              headers: {
              'Accept': 'application/json',
              'Authorization':'Bearer '+token 
              }
            })

      
dispatch({type:"getsubcate",payload:data})


    
}

}

// export  function getvideo(id) {
//   return async(dispatch)=>{
//       let token=localStorage.getItem('token');
  
//       const result=await axios.get("http://localhost:8000/api/getCourse/"+id,{
//         headers: {
//         'Accept': 'application/json',
//         'Authorization':'Bearer '+token 
//         }
//       })
    

// console.log(result)
//   dispatch({type:"getvideo",payload:result})


      
//   }
//}
 

