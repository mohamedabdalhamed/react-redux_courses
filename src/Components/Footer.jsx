import React,{useEffect,useState} from 'react'
import './footer.css'
function Footer() {
    const[token,settiken]=useState()
    useEffect(()=>{
        let tokens=localStorage.getItem('token')
        settiken(tokens)
    },)
  return (
        
<div className="footer mt-5 p-5">
    <div className='container'>
<div  className='row'>
<div className='col-md-4'>
<ul className="list-unstyled mt-5">
   <li><h3>Home</h3></li>
   <li><h3>About</h3></li>
   {token  ? <>
    <li><h3>Profile</h3></li>
   <li><h2>Course</h2></li>
   <li><h3>like</h3></li></> : 
    <>
    <li><h3>Login</h3></li>
   <li><h2>Register</h2></li>
</>  }

    
</ul>
</div>
<div className='col-md-4'>
<ul className="list-unstyled mt-5">
   <li><h3>Home</h3></li>
   <li><h3>About</h3></li>
   {token  ? <>
    <li><h3>Profile</h3></li>
   <li><h2>Course</h2></li>
   <li><h3>like</h3></li></> : 
    <>
    <li><h3>Login</h3></li>
   <li><h2>Register</h2></li>
</>  }

    
</ul>
</div>
<div className='col-md-4 mt-5'>
<h2>
    <i className="fa-brands fa-facebook-f m-1"></i> 
    <i className="fa-brands fa-instagram m-1"></i> 
    <i className="fa-brands fa-twitter m-1" ></i>
    <i className="fa-brands fa-whatsapp m-1"></i>
    
</h2>
<p>React makes it painless to create interactive UIs. Design simple views for each state in your application, and React will efficiently update and render just the right components when your data changes.</p>
</div>
</div>
</div>
</div>


  )
}

export default Footer