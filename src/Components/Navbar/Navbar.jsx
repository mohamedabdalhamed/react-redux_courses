import React,{useEffect,useState} from 'react'
import {connect} from 'react-redux';
import { actionsnews } from './../redux/actions';
import { NavLink,useHistory} from 'react-router-dom';
import axios from 'axios';
import {  toast } from 'react-toastify';
import jwt_decode from "jwt-decode";


function Navbar(props) {
  const[getsreach,setsreach]=useState();
  const[type,settype]=useState()

  const getuser=async()=>{
      let token=localStorage.getItem('token')
      var decoded = jwt_decode(token);
      let axiosConfig = {
          headers: {
              'Accept': 'application/json',
              'Authorization':'Bearer '+token 
              }
        };
      let {data}=await axios.get('http://localhost:8001/api/getusers/'+decoded.sub,axiosConfig)
      if(data.message==="success")
      {
        settype(data.type)
     
     
      }


  }
  
 useEffect(()=>
 {
  let token=localStorage.getItem('token')

   if(token){
  getuser();
    }
  props.actionsnews()
 },[ ]) 
 let history = useHistory();

 const  logout=()=>{
  getuser();
  props.actionsnews()

  localStorage.clear();
  getuser();
history.replace('/login')

}
const sreach=async (e)=>{
  var value=e.currentTarget.value;
  if(value===""){
    setsreach('')
    console.log('sdssdsda',getsreach)
  }else{
    var token = localStorage.getItem('token')
    const {data}=await axios.get("http://localhost:8001/api/sreach/"+value,{
              headers: {
              'Accept': 'application/json',
              'Authorization':'Bearer '+token 
              }
            })
          
      if(data.message==="success")
      {
        var  newObj= [...Object.values(data.catgoery)]
        setsreach(newObj);
        console.log(getsreach)
      } 
  }

}
  return (
    <div>
    <nav className="navbar navbar-expand-lg navbar-light navbac">
<div className="container-fluid">

<NavLink className="nnavbar-brand text-white" aria-current="page" to="home">Navbar</NavLink>
<button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
<span className="navbar-toggler-icon"></span>
</button>
<div className="collapse navbar-collapse" id="navbarSupportedContent">

{ localStorage.getItem('token')  ?     

<>
<ul className="navbar-nav me-auto mb-2 mb-lg-0">
<li className="nav-item">
<NavLink className="nav-link active text-white" aria-current="page" to="/home">Home</NavLink>
</li>

<li className="nav-item pr-5">
<NavLink className="nav-link actived text-white" aria-current="page" to="/profile">{props.home.name}</NavLink>
</li>

<li className="nav-item pr-5">
<NavLink className="nav-link actived text-white" aria-current="page" to="/all">All Course</NavLink>
</li>
<li className="nav-item pr-5">
<NavLink className="nav-link active text-white" aria-current="page" to="/like">like</NavLink>
</li>
<li className="nav-item pr-5">

{ type === 'administrator'  ? 


<NavLink className="nav-link active text-white" aria-current="page" to="/Admin">Admin</NavLink> 
 :<NavLink className="nav-link active text-white" aria-current="page" to="/User">Users</NavLink>

}

</li>
</ul>

    <div className="sreachparaen">
    <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" onChange={sreach} />
    <div className="Sreach">
    {getsreach ?
     <ul>
      {getsreach.map((value) => (<li key={value.id}><NavLink to={"/detailsCourse/"+value.id}> {value.nameCatgorey}</NavLink> </li>))}
    </ul> 
    :
    '' } 
    </div>
    </div>
    <NavLink  onClick={logout} className="nav-link active text-white" aria-current="page" to="login">logout  </NavLink>

    </>
:
<ul className="navbar-nav me-auto mb-2 mb-lg-0">
<li className="nav-item"> <NavLink className="nav-link text-white"  to="login">Login  </NavLink></li> 

<li className="nav-item" > <NavLink className="nav-link text-white"  to="register">Register  </NavLink></li> 
</ul>


}
</div>
</div>
</nav>
</div>
  )




}


function mapStateToProps(state){
  return {home:state.storedata}
}
 
export default connect(mapStateToProps,{actionsnews})(Navbar)
