import React,{useState,useEffect} from 'react'
import { toast } from 'react-toastify';
import  axios  from 'axios';
import $ from 'jquery';

function Catgorey() {
const[name,infomartion]=useState()
const[file,setimg]=useState()
const[show,getshow]=useState([])
const[Delete,deleteid]=useState()
const[edits,editmap]=useState([ ])
const[editid,seteditid]=useState([ ])

const images=(e)=>{
 
    let file=e.target.files[0]
    setimg(file)
    console.log(file)
}
const getinfo= (e)=>{
 
    infomartion(e.target.value)
    console.log(name)

   }
 const submit=async(e)=>{
 e.preventDefault();
let token=localStorage.getItem('token')
let format=new FormData()
format.append('file',file)
format.append('name',name)
let axiosConfig = {
    headers: {
        'Accept': 'application/json',
        'Authorization':'Bearer '+token 
        }
  };
let {data}=await axios.post("http://localhost:8001/api/catgoeryapi",format,axiosConfig)
if(data.message==='success')
{
    getinforow()
    toast.info('success',{ theme: "light" })


    $('.modal-backdrop').hide()  
    $('#exampleModal').hide()  

}
}
const getinforow=async()=>{
    let token=localStorage.getItem('token')
    let axiosConfig = {
        headers: {
            'Accept': 'application/json',
            'Authorization':'Bearer '+token 
            }
      };
    let {data}=await axios.get("http://localhost:8001/api/getposts",axiosConfig)
    if(data.message==='success')
    {
        getshow(data.catgoery);

    }
}
      const deletemodel=(id)=>
      {
        deleteid(id)
  
      }
      const newdelete=async()=>{
        let token=localStorage.getItem('token')
    let axiosConfig = {
        headers: {
            'Accept': 'application/json',
            'Authorization':'Bearer '+token,
          
            }
      };

    let {data}=await axios.delete("http://localhost:8001/api/deletcat/"+Delete,axiosConfig)
    if(data.message==='success')
    {
      getinforow()
      toast.error('done',{ theme: "light" } )
      
      $('.modal-backdrop').hide()  
      $('#deleteModal').hide()  
  

    }
      }
      const edit=async(id)=>{
        let token=localStorage.getItem('token')
        seteditid(id)
        let axiosConfig = {
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+token,
              
                }
          };
    
        let {data}=await axios.get("http://localhost:8001/api/edits/"+id,axiosConfig)
        if(data.message==='success')
        {
          editmap(data.Catgoery)
    
        }
      }
    const submitedit =async(e)=>{
if(!name && !file ){
  toast.info('nochanges',{ theme:"light"})
  $('#exampleModal').modal('hide')

}else{
  e.preventDefault();
  let token=localStorage.getItem('token')

  let format=new FormData()

  if(file){
  format.append('file',file)
  format.append('name',name)
}else{
  format.append('name',name)


}
  let axiosConfig = {
      headers: {
          'Accept': 'application/json',
          'Authorization':'Bearer '+token 
          }
    };
  let {data}=await axios.post("http://localhost:8001/api/editsend/"+editid,format,axiosConfig)
  if(data.message==='success')
  {
      getinforow()
      toast.info('success',{ theme: "light" })


    $('.modal-backdrop').hide()  
    $('#editModal').hide()  

  
  }
}

    }
useEffect(()=>{
    getinforow()
},[])

  return (
    <div className="container-fluid tables">
<button type="button" className="btn btn-primary m-4" data-bs-toggle="modal" data-bs-target="#exampleModal">
Add New</button>

   
     
        <table className="table">
  <thead> 
    <tr>
      <th scope="col">#</th>
      <th scope="col">name</th>
      <th scope="col">Images</th>
      <th scope="col">Control</th>
    </tr>
  </thead>
  <tbody>
  
{show.map((value)=>( 
        <tr key={value.id}>

<th scope="row">{value.id}</th>
      <td>{value.nameCatgorey}</td>
      <td>
          <img src={`http://localhost:8001/uploads/catge/${value.img}`} width="50" height="50" /></td>
      <td> 
      <button type="button" className="btn btn-danger m-1" data-bs-toggle="modal" data-bs-target="#deleteModal"  onClick={()=>deletemodel(value.id)}>delete</button>
      <button type="button" className="btn btn-success" data-bs-toggle="modal" data-bs-target="#editModal" onClick={()=>edit(value.id)}>Edit</button>


    </td>
     
      </tr>
     ))}
  
  </tbody>
</table>
{/* start model delete */}



    <div className="modal fade" id="deleteModal"  aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div className="modal-dialog">
    <div className="modal-content">
      <div className="modal-header">
        <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div className="modal-body">
      are You Sure?
  </div>
      <div className="modal-footer">
        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button  className="btn btn-Danger" onClick={()=>newdelete()}><i className="fa-solid fa-trash fa-2x"></i>Delete</button>
      </div>
    </div>
  </div>

</div>

{/* start model EDIT */}
    <div className="modal fade" id="editModal"  aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form onSubmit={submitedit}>
  <div className="modal-dialog">
    <div className="modal-content">
      <div className="modal-header">
        <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div className="modal-body">
      <input placeholder="Type Title" name="name" className="form-control" defaultValue={edits.nameCatgorey} onChange={getinfo} type="text"/>
      <input placeholder="Type Title" name="file" multiple className="form-control mt-4" defaultValue={edits.img} type="file" onChange={images} />
  </div>
      <div className="modal-footer">
        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" className="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
  </form>

</div>

 {/* model add */}



    <div className="modal fade" id="exampleModal"  aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form onSubmit={submit}>
  <div className="modal-dialog">
    <div className="modal-content">
      <div className="modal-header">
        <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div className="modal-body">
      <input placeholder="Type Title" name="name" className="form-control"onChange={getinfo}type="text"/>
                        <input placeholder="Type Title" name="file" multiple className="form-control mt-4" type="file" onChange={images} />
      </div>
      <div className="modal-footer">
        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" className="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
  </form>

</div>

    </div>
  )
}

export default Catgorey