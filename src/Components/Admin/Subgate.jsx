import React,{useState,useEffect} from 'react'
import axios from 'axios'
import { toast } from 'react-toastify';
import $,{modal} from 'jquery';

function Subgate() {
    const[show,getshow]=useState([])
    const[file,setfile]=useState([])
    const[getinf,setinfo]=useState({name:'',cate:''})
    const[showinfo,setsubget]=useState([])
    const[iddelete,setdeletid]=useState()
    const[edits,editmap]=useState([ ])
    const[editids,seteditid]=useState([ ])
 
    const getinforow=async()=>{
        let token=localStorage.getItem('token')
        let axiosConfig = {
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+token 
                }
          };
        let {data}=await axios.get("http://localhost:8001/api/getposts",axiosConfig)
        if(data.message==='success')
        {
            getshow(data.catgoery);
    
        }
      }
     const getinfo=(e)=>{
let ss={...getinf};
 ss[e.currentTarget.name]=e.currentTarget.value;
 setinfo(ss);
 console.log(ss)

     }
     const getfiles=(e)=>{
     setfile(e.currentTarget.files[0]);    
     }
     const submit=async(e)=>{
         let format=new FormData();
         format.append('file',file)
         format.append('name',getinf.name)
         format.append('cate',getinf.cate)
         e.preventDefault();
         let token=localStorage.getItem('token')

         let axiosConfig = {
             headers: {
                 'Accept': 'application/json',
                 'Authorization':'Bearer '+token 
                 }
           };
         let {data}=await axios.post("http://localhost:8001/api/subcate",format,axiosConfig)
         if(data.message==='success')
         {  
            showinformationsubgetory()
            toast.info('success',{ theme: "light" })
          
    $('.modal-backdrop').hide()  
    $('#exampleModal').hide()  
               
         }
     }
     const showinformationsubgetory=async()=>{
        let token=localStorage.getItem('token')
        let axiosConfig = {
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+token 
                }
          };
        let {data}=await axios.get("http://localhost:8001/api/showcate",axiosConfig)
        if(data.message==='success')
        {
            setsubget(data.info);
    
        }
      }
      const deletemodel=(id)=>{
        setdeletid(id)

      }
   
      const newdelete=async()=>{
        let token=localStorage.getItem('token')
    let axiosConfig = {
        headers: {
            'Accept': 'application/json',
            'Authorization':'Bearer '+token,
          
            }
      };

    let {data}=await axios.delete("http://localhost:8001/api/deletesubcat/"+iddelete,axiosConfig)
    if(data.message==='success')
    {
        showinformationsubgetory()
      toast.error('done',{ theme: "light" } )
    
      $('.modal-backdrop').hide()  
      $('#deleteModal').hide()  
    }
      }
      const edit=async(id)=>{
        let token=localStorage.getItem('token')
        seteditid(id)
        let axiosConfig = {
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+token,
              
                }
          };
    
        let {data}=await axios.get("http://localhost:8001/api/editsubcate/"+id,axiosConfig)
        if(data.message==='success')
        {
          editmap(data.subCategoery)
    
        }
      }
    const submitedit =async(e)=>{
        if(!getinf.name && !file ){
          toast.info('nochanges',{ theme:"light"})
    
          $('.modal-backdrop').hide()  
          $('#editModal').hide()         
        }else{
          e.preventDefault();
          let token=localStorage.getItem('token')
        
          let format=new FormData()
      
            
            format.append('file',file)
            console.log('right')
        if(getinf.name){
            format.append('name',getinf.name)
        }else{
            format.append('name',edits.name)

        }
        if(getinf.cate){
            format.append('cate',getinf.cate)

        }else{
            format.append('cate',edits.numberCategoery_id)

        }
         
          let axiosConfig = {
              headers: {
                  'Accept': 'application/json',
                  'Authorization':'Bearer '+token 
                  }
            };
          let {data}=await axios.post("http://localhost:8001/api/sendeditsubcate/"+editids,format,axiosConfig)
          if(data.message==='success')
          {
            showinformationsubgetory()
              toast.info('success',{ theme: "light" })
         

    $('.modal-backdrop').hide()  
    $('#editModal').hide()  

          
          }
        }
        
            }

      useEffect(()=>{
        getinforow()
        showinformationsubgetory()
      },[])
  return (
    <div>
    <div className="container-fluid tables">

    <button type="button" className="btn btn-primary m-4" data-bs-toggle="modal" data-bs-target="#exampleModal">
Add New</button>

   <table className="table  ">
<thead> 
<tr>
 <th scope="col">#</th>
 <th scope="col">name</th>
 <th scope="col">Images</th>
 <th scope="col">Control</th>
</tr>
</thead>
<tbody>

{ showinfo.map((value)=>( 
   <tr key={value.id}>

<th scope="row">{value.id}</th>
 <td>{value.name}</td>
 <td>
     <img src={`http://localhost:8001/uploads/catge/${value.imges}`} width="50" height="50" /></td>
 <td>  

  <button type="button" className="btn btn-danger m-1" data-bs-toggle="modal" data-bs-target="#deleteModal"  onClick={()=>deletemodel(value.id)}>delete</button>
      <button type="button" className="btn btn-success" data-bs-toggle="modal" data-bs-target="#editModal" onClick={()=>edit(value.id)}>Edit</button>
 
</td>

 </tr>
))} 


</tbody>
</table>

{/* start model delete */}
<div className="modal fade" id="deleteModal"  aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
                    </div>
                    <div className="modal-body">
                      are You Sure?
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button className="btn btn-Danger" onClick={()=>newdelete()}><i className="fa-solid fa-trash fa-2x"></i>Delete</button>
                    </div>
                </div>
            </div>
    </div>

{/* start model EDIT */}
<div className="modal fade" id="editModal"  aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form onSubmit={submitedit}>
  <div className="modal-dialog">
    <div className="modal-content">
      <div className="modal-header">
        <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div className="modal-body">
      <input placeholder="Type Title" name="name" className="form-control" defaultValue={edits.name} onChange={getinfo} type="text"/>
 
 <select  name="cate" className="form-control mt-4"  onChange={getinfo}>
   <option value="DEFAULT" >none</option>
  {show.map((value)=> {

  return (<option value={value.id}    
         key={value.id}>{value.nameCatgorey}</option>)
                      })}
 </select>

 <input placeholder="Type Title" name="file" multiple className="form-control mt-4" defaultValue={edits.imges} type="file" onChange={getfiles} />

  </div>
      <div className="modal-footer">
        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" className="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
  </form>

</div>


 {/* model add */}



 <div className="modal fade" id="exampleModal"  aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form onSubmit={submit}>
  <div className="modal-dialog">
    <div className="modal-content">
      <div className="modal-header">
        <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div className="modal-body">
      <input placeholder="Type Title" name="name" onChange={getinfo}  className="form-control"type="text"/>
                 
                 <select  name="cate" className="form-control mt-4" onChange={getinfo}>
                   <option value="" >none</option>
                  {show.map((value)=>( 
                     <option value={value.id} key={value.id}>{value.nameCatgorey}</option>
                ))}
                 </select>
                 <input placeholder="Type Title" name="file"  multiple onChange={getfiles}  className="form-control mt-4" type="file"  />
      </div>
      <div className="modal-footer">
        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" className="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
  </form>

</div>

</div>

</div>
  )
}

export default Subgate

