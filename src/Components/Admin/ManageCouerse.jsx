import React,{useEffect, useState} from 'react'
import axios from 'axios';
import { toast } from 'react-toastify';
import $ from 'jquery';
function ManageCouerse() {
    const[showselect,setsubget]=useState([])
    const[file,setfileimage]=useState()
    const[video,setvideo]=useState([])
    const[showcouresed,setshowcourese]=useState([])
    const[getallifni,setinfos]=useState({name:'',cate:'',numberofvideo:0,desc:'',age:0})
    const[iddelete,setdeletid]=useState()
    const[edits,editmap]=useState([ ])
    const[editids,seteditid]=useState([ ])
    
    const showcoureses=async()=>{
        let token=localStorage.getItem('token')
        let axiosConfig = {
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+token 
                }
          };
        let {data}=await axios.get("http://localhost:8001/api/showcourese",axiosConfig)
        if(data.message==='success')
        {
            var newObj = [...Object.values(data.course)]

            setshowcourese(newObj);
    
        }
      }
    const showinformationsubgetory=async()=>{
        let token=localStorage.getItem('token')
        let axiosConfig = {
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+token 
                }
          };
        let {data}=await axios.get("http://localhost:8001/api/showcate",axiosConfig)
        if(data.message==='success')
        {
            setsubget(data.info);
    
        }
      }
    const getfiles=(e)=>{
        setvideo(e.currentTarget.files)
        console.log(video)
    }
    const getfile=(e)=>{
        setfileimage(e.currentTarget.files[0])
        console.log(file)
    }
    const getinfo=(e)=>{
var ss={...getallifni}
  ss[e.currentTarget.name]=e.currentTarget.value
  setinfos(ss)
  console.log(getallifni)

}
    const submit=async(e)=>{
        e.preventDefault()
        let foramt=new FormData()
   
    for(const file of video )
    {
        foramt.append('video[]',file)
    }

    foramt.append('name',getallifni.name)
    foramt.append('cate',getallifni.cate)
    foramt.append('numberofvideo',getallifni.numberofvideo)
    foramt.append('file',file)
    foramt.append('desc',getallifni.desc)
    foramt.append('age',getallifni.age)
    
    let token=localStorage.getItem('token')

    let axiosConfig = {
        headers: {
            'Accept': 'application/json',
            'Authorization':'Bearer '+token 
            }
      };
    console.log(foramt)
      let {data}=await axios.post("http://localhost:8001/api/courese",foramt,axiosConfig)
    if(data.message==='success')
    {  showcoureses()
       toast.info('success',{ theme: "light" })
       $('.modal-backdrop').hide()  
       $('#exampleModal').hide() 

                      
    }

}
const deletemodel=(id)=>{
    setdeletid(id)

  }
  const newdelete=async()=>{
    let token=localStorage.getItem('token')
let axiosConfig = {
    headers: {
        'Accept': 'application/json',
        'Authorization':'Bearer '+token,
      
        }
  };

let {data}=await axios.delete("http://localhost:8001/api/destroyscourses/"+iddelete,axiosConfig)
if(data.message==='success')
{
    showcoureses()
  toast.error('done',{ theme: "light" } )

  $('.modal-backdrop').hide()  
  $('#deleteModal').hide()  
}
  }
  

  const edit=async(id)=>{
    let token=localStorage.getItem('token')
    seteditid(id)
    let axiosConfig = {
        headers: {
            'Accept': 'application/json',
            'Authorization':'Bearer '+token,
          
            }
      };

    let {data}=await axios.get("http://localhost:8001/api/editcourse/"+id,axiosConfig)
    if(data.message==='success')
    {
      editmap(data.couses)

    }
  }



    useEffect(()=>{
        showcoureses()

        showinformationsubgetory()


    },[])
  return (
    <div>    <div className="container-fluid tables">

<button type="button" className="btn btn-primary m-4" data-bs-toggle="modal" data-bs-target="#exampleModal">
Add New</button>
    
       <table className="table ">
    <thead> 
    <tr>
     <th scope="col">#</th>
     <th scope="col">name</th>

     <th scope="col">numberOfVideo</th>
     <th scope="col">describtion</th>
     <th scope="col">age</th>
     <th scope="col">image</th>
     <th scope="col">Control</th>
    </tr>
    </thead>
    <tbody>
    
    { showcouresed.map((value)=>
       <tr key={value.id}>
    <th scope="row">{value.id}</th>
     <td>{value.name}</td>
     <td>{value.numberOfVideo}</td>
     <td>{value.describtion}</td>
     <td>{value.age}</td>
     
     <td>
         <img src={`http://localhost:8001/uploads/${value.imges}`} width="50" height="50" /></td>
     <td>  
   
   
  <button type="button" className="btn btn-danger m-1" data-bs-toggle="modal" data-bs-target="#deleteModal"  onClick={()=>deletemodel(value.id)}>delete</button>
      <button type="button" className="btn btn-success" data-bs-toggle="modal" data-bs-target="#editModal" onClick={()=>edit(value.id)}>Edit</button>
 
    </td>
     </tr>
    )} 
    
    
    </tbody>
    </table>

    <div className="modal fade" id="deleteModal"  aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
                    </div>
                    <div className="modal-body">
                      are You Sure?
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button className="btn btn-Danger" onClick={()=>newdelete()}><i className="fa-solid fa-trash fa-2x"></i>Delete</button>
                    </div>
                </div>
            </div>
    </div>


    <div className="modal fade" id="editModal"  aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form onSubmit={submit}>
  <div className="modal-dialog">
    <div className="modal-content">
      <div className="modal-header">
        <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div className="modal-body">
      <input placeholder="Type Title" name="name" onChange={getinfo} defaultValue={edits.name}  className="form-control"type="text"/>
                 
                 <select  name="cate" className="form-control mt-4" onChange={getinfo}>
                   <option value="" >none</option>
                  {showselect.map((value)=>( 
                     <option value={value.id} key={value.id}>{value.name}</option>
                ))}
                 </select>
                 <input placeholder="Number of video" name="numberofvideo" onChange={getinfo}  defaultValue={edits.numberOfVideo} className="form-control mt-4 "type="Number"/>
                 <input placeholder="Type Title" name="file"  multiple onChange={getfile}  className="form-control mt-4" type="file"  />
                 <textarea className='w-100  mt-4' name="desc" rows="4" cols="100" defaultValue={edits.describtion} onChange={getinfo}>

  </textarea>
  <input placeholder="Type video" name="video"  multiple onChange={getfiles}  className="form-control mt-4" type="file"  />

  <input placeholder="ages" name="age" onChange={getinfo}  className="form-control  mt-4"type="Number"/>


      </div>
      <div className="modal-footer">
        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" className="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
  </form>

</div>


    
{/* model add */}

<div className="modal fade" id="exampleModal"  aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form onSubmit={submit}>
  <div className="modal-dialog">
    <div className="modal-content">
      <div className="modal-header">
        <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div className="modal-body">
      <input placeholder="Type Title" name="name" onChange={getinfo}  className="form-control"type="text"/>
                 
                 <select  name="cate" className="form-control mt-4" onChange={getinfo}>
                   <option value="" >none</option>
                  {showselect.map((value)=>( 
                     <option value={value.id} key={value.id}>{value.name}</option>
                ))}
                 </select>
                 <input placeholder="Number of video" name="numberofvideo" onChange={getinfo}  className="form-control mt-4 "type="Number"/>
                 <input placeholder="Type Title" name="file"  multiple onChange={getfile}  className="form-control mt-4" type="file"  />
                 <textarea className='w-100  mt-4' name="desc" rows="4" cols="100"  defaultValue=" " onChange={getinfo}>

  </textarea>
  <input placeholder="Type video" name="video"  multiple onChange={getfiles}  className="form-control mt-4" type="file"  />

  <input placeholder="ages" name="age" onChange={getinfo}  className="form-control  mt-4"type="Number"/>


      </div>
      <div className="modal-footer">
        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" className="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
  </form>

</div>

    </div>
    </div>

  )
}

export default ManageCouerse