import React, { useState, useEffect,useRef} from "react";
import imges from '../../imges/download.jpg'
import center2 from '../../imges/center2.png'
import axios from "axios";
import {  toast } from 'react-toastify';
import { NavLink } from "react-router-dom";
import './videoscam.css'

import About from "../About/About";

export default function Home() {
  const [count, setCount] = useState([]);

  const cou= async()=>{
    let token=localStorage.getItem('token');

    let {data}=await axios.get("http://localhost:8001/api/categoery",{
      headers: {
      'Accept': 'application/json',
      'Authorization':'Bearer '+token 
      }
    } );

    if(data.message==="success")
    {
      
      var  newObj= [...Object.values(data.catgoery)]
      setCount(newObj)

       console.log(data.catgoery)
    } else{
      toast("Error")
    }
    }     
    
  
    
  useEffect(() => {
    cou()

  },[]);

  return (
    <div className="home">
<div id="carouselExampleCaptions" className="carousel slide" data-bs-ride="carousel">
  <div className="carousel-indicators">
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
  </div>
  <div className="carousel-inner">

    <div className="carousel-item active  ">
      <img src={center2} className="d-block w-100 vh-100" alt="..."/>
      <div className="carousel-caption d-none d-md-block">
        <h5 className="colors" >Second slide label</h5>
        <p className="colors">Some representative placeholder content for the second slide.</p>
      </div>
    </div>
   
  </div>
  <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
    <span className="visually-hidden">Previous</span>
  </button>
  <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
    <span className="carousel-control-next-icon" aria-hidden="true"></span>
    <span className="visually-hidden">Next</span>
  </button>
</div>

<div className='container'>
<About/>

  <h1 className="text-center text-dark my-5"> Cour<span className='colors'>se</span> </h1>
  <div className='row'>
  {count.map((value)=>{
  return (<div className='col-md-3 col-sm-12 mt-3 items' key={value.id}>
  <div className="card" style={{"width": "18rem"}}>
  <img  src={`http://localhost:8001/uploads/catge/${value.img}`} className="card-img-top" alt="..."/>
  
    <div className="card-body">
    <h5 className="card-title">{value.nameCatgorey}</h5>
    <NavLink to={"detailsCourse/"+value.id}className="btn buton my-2">
      See More
    </NavLink>
{/* <p className="text-small">Updated At:{value.updated_at}
</p> */}
  </div>
</div>
  </div>)
  
  })}

</div> 
</div>

                   </div>
  )
}