import React,{useEffect,useState} from 'react'
import { useParams } from 'react-router-dom'
import {  toast } from 'react-toastify';
import { NavLink } from "react-router-dom";
import axios from 'axios';
import Buttons from '../../Course/Buttons';



 function DetailsCourse(props) {
    let { id } = useParams();
const[cate,setdata]=useState([]);

const datasetcat=async ()=>{
    var token = localStorage.getItem('token')
const {data}=await axios.get("http://localhost:8001/api/subCatgeorys/"+id,{
            headers: {
            'Accept': 'application/json',
            'Authorization':'Bearer '+token 
            }
          })
        
    if(data.message==="success")
    {
   
       var newObj = [...Object.values(data.subcategoery)]

      setdata(newObj)
   
    } else{
      toast("Error");

    }


  }


useEffect(()=>{
datasetcat()
          

},[])

  return (
    <div className='container tables'>

    <h1 style={{ color:"#fff" }} className="text-center">Course</h1>
<div className='row'>

        {cate.map(value => (

          
<div className='col-md-3 col-sm-12 items' key={value.id}>
    <div className="card" style={{"width": "18rem"}}>

  <img  src={`http://localhost:8001/uploads/catge/${value.imges}`} width="161" height="272" className="card-img-top" alt="..."/>
  
          
  
    <div className="card-body">
    <h5 className="card-title">{value.name}</h5>
    {/* <small className="text-muted">Number of VIDEO:{value.numberOfVideo}</small>

<small className="text-muted">{value.created_at}</small> */}
  
  <Buttons id={value.id} />

  </div>
</div>
</div>

      ))};

  </div>
    </div>
  )
}

export default  DetailsCourse