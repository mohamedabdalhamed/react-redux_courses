import React,{useEffect,useState} from 'react'
import axios from 'axios'
import { NavLink } from 'react-router-dom';

function AllLikes() {
    const [like,setlike]=useState([]);
    const getlike=async()=>{
        let token=localStorage.getItem('token')
        let axiosConfig = {
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+token 
                }
          };
        let {data}=await axios.get('http://localhost:8001/api/showlike',axiosConfig)
        if(data.message==="success"){
           
            setlike(data.like) 
             console.log(data.like)  
        }
    }
    useEffect(()=>{
        getlike()
    },[])
  return (
    <div className={'container  tables'}>
        <h1 className="text-center text-light">All yout likes</h1>
       {like.map((value,index)=>(
            <div className="row mt-5 border-bottom" key={index}>
            <div className="col col-md-3">
                <h3 className='text-light'>NameCourse:{value.name}</h3>
               <NavLink to={"/myCourse/"+value.category+"/Donevideo/"+value.numbercourse+"/"+value.video.substring(0, value.video.indexOf('.mp4'))       }> <img width={'300px'} height={'300px'} src={"http://localhost:8001/uploads/"+value.imges} /></NavLink>
                <p className='text-light'>{value.describtion}</p>

            </div>

            <div className="col offset-md-7">
                <h3 className="text-light">time</h3>
                <p className="text-light">{value.crea}</p>
            </div>
        </div>
        ) 
        )}

    </div>
  )
}

export default AllLikes