
import React,{useEffect,useState} from 'react'
import { useParams } from 'react-router-dom'
import {  toast } from 'react-toastify';
import { NavLink ,useHistory} from "react-router-dom";
import axios from 'axios';

export default function Buttons(props) {
    const[databutton,setsa]=useState([]);

    const joinOrCour= async()=>{
        let token=localStorage.getItem('token');
      
        const result=await axios.get("http://localhost:8001/api/getCourse/"+props.id,{
          headers: {
          'Accept': 'application/json',
          'Authorization':'Bearer '+token 
          }
        })
      
        if(result.data.getvideo.length===0){
            setsa(0)
        }else{
            setsa(1)

        }

    }


      useEffect(()=>{
        joinOrCour()
                  
        
        },[])
  return (
    <div>


    {databutton === 0 ?  
<NavLink className="btn btn-primary" to={"/Addcourse/"+props.id}>
   Go add
   </NavLink> 
   :
   <NavLink  className="btn btn-primary" to={"/myCourse/"+props.id}>
Course
   </NavLink> }
    </div>
  )
}
