import React,{useEffect,useState} from 'react'
import ReactPlayer from 'react-player'
import { useParams } from 'react-router-dom';
import './video.css'
import axios from "axios";
import Like from './Like/Like';
import Comment from './Like/Comment';
function Donevideo() {

    let {id,ids}=useParams();
              
     
  return (
    <>
  
  <ReactPlayer controls 
  light={true} 
  width={"100%"}
   height={"100%"}
    url={"http://localhost:8001/uploads/videos/"+id+".mp4"}
    onReady={()=> console.log('On Ready Callback')}
    onStart={()=> console.log('On onStart Callback')}
    onPause={()=> console.log('On onPause Callback')}
    onEnded={()=> console.log('On onEnded Callback')}
    onError={()=> console.log('On onError Callback')}
    
    />  
    {ids}   
    <Like ide={ids} />
   
<Comment ide={ids} />

      </>
  )
}

export default Donevideo