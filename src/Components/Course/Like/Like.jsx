import React,{useEffect,useState} from 'react'
    import axios from "axios";
function Like(props) {
    
const[likes,setlike]=useState(false);
          
    const getlike=async ()=>{
        let token=localStorage.getItem('token');

        var {data}= await axios.get('http://localhost:8001/api/showfav/'+props.ide,
      {
        headers: {
        'Accept': 'application/json',
        'Authorization':'Bearer '+token 
        }
      })

      if(data.message==="success"){
        setlike(true) 
        console.log(props.ide)
      }else{        setlike(false)      }

      }

     const icons =async ()=>{
      let val=likes;
      let token=localStorage.getItem('token');
      setlike(!val);

      var {data}= await axios.get('http://localhost:8001/api/fav/'+props.ide,
      {
        headers: {
        'Accept': 'application/json',
        'Authorization':'Bearer '+token 
        }
      })
    
    if(data.message==="ERROR"){
      console.log(data.message)
    } else{
      console.log(data.message)

    }
     
    }  
     
    
   useEffect(() => {
    getlike()
  console.log(props.ide,"test")
  },[props.ide]);


  return (
    <div className='d-flex justify-content-between'>
    {likes===true ? <i className={'fa-solid fa-thumbs-up fa-3x my-2 colors  '} onClick={icons} ></i>   : <i className={'fa-solid fa-thumbs-up  fa-3x my-2'} onClick={icons} ></i> }
</div> 
  )
}

export default Like