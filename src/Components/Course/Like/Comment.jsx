import React,{useEffect,useState} from 'react'
import axios from "axios";

import {  toast } from 'react-toastify';
import Pagination from "react-js-pagination";
import './stylecomment.css'
import jwt_decode from "jwt-decode";
import imges from '../../../imges/2.png'
function Comment(props) {
    const [comment, setcomment] = useState([])
    const[activePage,setactivePage]=useState();
    const[total,settotal]=useState();
    const[per_page,setper_page]=useState();
    const[DECP,SECdeco]=useState();
    const [datas, setData] = useState()

    const GetInptDate=(e)=>{
        setData(e.target.value)

    }

    const getsubmit=async(e)=>{

        e.preventDefault();
        let token=localStorage.getItem('token');
        var ids=props.ide;
        let axiosConfig = {
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+token 
                }
          };
        const {data}=await axios.post("http://localhost:8001/api/addcomment",{ids:ids,comment:datas},axiosConfig)
        if(data.message==="success"){
            setData('')
            toast('Done comment')
            ShowComment()

        }
        
    }

    const  handlePageChange=async (pageNumber)=> {
        let token=localStorage.getItem('token');
        let {data}=await axios.get(`http://127.0.0.1:8001/api/comments/`+props.ide+`?page=${pageNumber}`,{
            headers: {
            'Accept': 'application/json',
            'Authorization':'Bearer '+token 
            }
          });
          if(data.message==="success"){
          
            setcomment(data.comments.data)
  settotal(data.comments.total)
            setper_page(data.comments.per_page)
      
          }
      setactivePage( pageNumber);
      }

    const ShowComment=async()=>{

        let token=localStorage.getItem('token');
        let axiosConfig = {
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+token 
                }
          };
        const {data}=await axios.get("http://localhost:8001/api/comments/"+props.ide,axiosConfig)
        if(data.message==="success"){
           
            setcomment(data.comments.data)
            settotal(data.comments.total)
            setper_page(data.comments.per_page)
      
        
        }
        
    }
    const deletecomment=async(id)=>{
        let token=localStorage.getItem('token');
        let axiosConfig = {
            headers: {
                'Accept': 'application/json',
                'Authorization':'Bearer '+token 
                }
          };
        const {data}=await axios.get("http://localhost:8001/api/DeleteComment/"+id,axiosConfig)
        if(data.message==="success"){
            console.log(data)

            ShowComment()
        }
    }
useEffect(()=>{
    ShowComment()

    let token=localStorage.getItem('token');
   var  d=jwt_decode(token);
   SECdeco(d.sub)


},[props.ide])
  return (
      <>

<form onSubmit={getsubmit}>
    
    { <textarea className='w-100  mt-5' name="comment" rows="4" cols="100" value={datas} defaultValue=" " onChange={GetInptDate}>
    </textarea> 
    }
    <br/>
    <input type="submit" className={"btn btn-primary my-4 coloricon text-center"} />
  </form>

  <div>
     {comment.map((value)=> (
         <div className="row border-bottom ml-1" key={value.id}>
             <div className='col-md-2 col-sm-12'>
            {value.image ?          <img src={`http://localhost:8001/uploads/${value.image}`}   style={{ width:'100px',height:'100px', borderRadius:'50%'}}  className="mx-4 my-4" />
 :           <img src={imges}  style={{ width:'100px',height:'100px', borderRadius:'50%'}}  className="mx-4 my-4" />
}
         
             </div>
             <div className='col-md-10 mt-5  col-sm-12  '>
             <h5 className="colors">{value.name} </h5>
             <div className='d-flex justify-content-between'>
             <p className="text-dark">{value.comment}</p>
  

{DECP==value.user_id ?             <i className='fa-solid fa-trash fa-2x' style={{color:'red',cursor: 'pointer'}} onClick={()=>deletecomment(value.id)}></i>
 :''}
             </div>
             </div>
         </div>
     ))}
    {total >0 ?  <div className="pangination">
         <Pagination
          activePage={activePage}
          itemsCountPerPage={per_page}
          totalItemsCount={total}
      
          itemClass="page-item"
          linkClass="page-link"
          onChange={handlePageChange}
        />
         </div>
 :
 ''
 }
      </div>
  </>
  )
}

export default Comment