  import React,{useEffect} from 'react'
import { NavLink, Route, useParams} from 'react-router-dom'
import {connect} from 'react-redux';
import { getsubcategoe,getuserfun } from './../redux/actions';
import './sidebar/sidebars'
import './sidebar/sidebars.css'
import Sidebarcom from './sidebar/Sidebarcom';
import Donevideo from './Donevideo';
import imges from '../../imges/2.png'

function MyCourse(props) {
let { id } = useParams();
useEffect(()=>{
  props.getsubcategoe(id)
  if(props.subcate.user_id===undefined){

  }else{
  props.getuserfun(props.subcate.user_id)
  console.log(props.userse.id)
  }
},[props.subcate.user_id])

  return ( <div >
 <main>
 <div  style={{"width": "15%"}} className="d-flex flex-column flex-shrink-0 p-3  bg-white w" >
    <a href="/" className="d-flex align-items-center mb-3 mb-md-0 me-md-auto  text-decoration-none">
      <span className="fs-4 colors">           {props.subcate.name}</span>
    </a>
    <hr/>
 
    <ul className="list-unstyled ps-0">
  
      <li className="mb-1">
        <Sidebarcom  IdCourse={props.subcate.id}/>
     </li>
    
      <li className="border-top my-3"></li>
      <li className="mb-1">
        <button className="btn btn-toggle align-items-center rounded collapsed" data-bs-toggle="collapse" data-bs-target="#account-collapse" aria-expanded="false">
          Account
        </button>
        <div className="collapse" id="account-collapse">
          <ul className="btn-toggle-nav list-unstyled fw-normal pb-1 small">
            <li><a href="#" className="link-dark rounded">New...</a></li>
            <li><a href="#" className="link-dark rounded">Profile</a></li>
            <li><a href="#" className="link-dark rounded">Settings</a></li>
            <li><a href="#" className="link-dark rounded">Sign out</a></li>
          </ul>
        </div>
      </li>
    </ul>
    <hr/>
    <div className="dropdown">
      <NavLink to={"/Rateandfollow/"  +props.userse.id} className="d-flex align-items-center text-white text-decoration-none dropdown-toggle"> 
         
      {props.userse.image ?        <img src={`http://localhost:8001/uploads/${props.userse.image}`}  width="32" height="32" className="rounded-circle me-2"/>
  :         <img src={imges}  width="32" height="32" className="rounded-circle me-2"/>
}
        
        
        <h4 className='colors mt-3'>{props.userse.name}</h4>
    
      </NavLink>
    
    </div>
  </div>

  <div className="b-example-divider"></div>
  <div  style={{"width": "80%"}} className="d-flex flex-column flex-shrink-0 p-3 w2">

    <Route path="/myCourse/:id/Donevideo/:ids/:id" component={Donevideo}/>

    
</div>
 </main>
        </div>

        
  )
}

function mapStateToProps(state){
  return {
    subcate:state.getsubcategoery,
    userse:state.users
    
  }
}

export default connect(mapStateToProps,{getsubcategoe,getuserfun})(MyCourse)

