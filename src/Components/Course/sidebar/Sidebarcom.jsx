import React,{useEffect,useState} from 'react'
import {connect} from 'react-redux';
import { course } from './../../redux/actions';
import axios from 'axios';
import Getvidei from './Getvidei';
function Sidebarcom(props) {
    const[getcours,setcours]=useState([])
    const getcou= async()=>{
        let token=localStorage.getItem('token');
  
        const result=await axios.get("http://localhost:8001/api/course/"+props.IdCourse,{
          headers: {
          'Accept': 'application/json',
          'Authorization':'Bearer '+token 
          }
        })
if(result.data.message==='success'){
  
  var newObj = [...Object.values(result.data.couses)]


    setcours(newObj)
}


    }
useEffect(()=>{
    getcou()
//      .course(props.IdCourse)
;
console.log(getcours)
  },[props])

  return ( <div>
    {getcours.map((value)=>(<div key={value.id}>
        <button className="btn btn-toggle align-items-center rounded collapsed" data-bs-toggle="collapse" data-bs-target={"#"+value.name} aria-expanded="false">
           {value.name}
        </button>
     
             <div className="collapse" id={value.name} >
               <ul className="btn-toggle-nav list-unstyled fw-normal pb-1 small">
                 
        <Getvidei id={value.id}  prop={props.IdCourse}  />  

               </ul>
             </div>
             </div>
          ))}
</div>
            )
}



  

function mapStateToProps(state){
  return {
      getcours:state.course
    
  }
}

  export default connect(mapStateToProps,{course})(Sidebarcom)
  
  
