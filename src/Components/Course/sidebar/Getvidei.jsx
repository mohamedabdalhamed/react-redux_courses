import React,{useState,useEffect} from 'react'
import { NavLink } from "react-router-dom";
import  axios  from 'axios';

function Getvidei(props) {
  const[videos,setvideo]=useState([])
 
  const getiditemvideo=async ()=>{
    let token =localStorage.getItem('token');
    const result=await axios.get('http://localhost:8001/api/getCoursevideo/'+props.id,{
      headers: {
        'Accept': 'application/json',
        'Authorization':'Bearer '+token 
        }
    }) 
var newObj = [...Object.values(result.data.videos)]
setvideo(newObj)
}
    useEffect(()=>{
      getiditemvideo();
      
    },[props.id])
  return (
    <div>

{videos.map((value,index)=>(

<li key={index}>

   <NavLink  className="link-dark rounded" to={"/myCourse/"+props.prop+"/Donevideo/"+value.id+"/"+value.video.substring(0, value.video.indexOf('.mp4'))       }> 
section {index}
 </NavLink>
 </li>
))}   


    </div>
  )
}

export default Getvidei