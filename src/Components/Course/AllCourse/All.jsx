import React,{useState,useEffect} from 'react'
import './allcourse.css'
import axios from 'axios'
import Pagination from "react-js-pagination";

export default function All() {
const[course,setcourse]=useState([]);
const[activePage,setactivePage]=useState();
const[total,settotal]=useState(1);
const[per_page,setper_page]=useState(1);
const  handlePageChange=async (pageNumber)=> {
  let token=localStorage.getItem('token');
  let {data}=await axios.get(`http://127.0.0.1:8001/api/getall?page=${pageNumber}`,{
      headers: {
      'Accept': 'application/json',
      'Authorization':'Bearer '+token 
      }
    });
    if(data.message==="success"){
    
      var  newObj= [...Object.values(data.catgoery.data)]
      setcourse(newObj)
      settotal(data.catgoery.total)
      setper_page(data.catgoery.per_page)

    }
setactivePage( pageNumber);
}

const Allcourse= async()=>{
    let token=localStorage.getItem('token');
    let {data}=await axios.get(`http://127.0.0.1:8001/api/getall`,{
        headers: {
        'Accept': 'application/json',
        'Authorization':'Bearer '+token 
        }
      });
      if(data.message==="success"){
      
        var  newObj= [...Object.values(data.catgoery.data)]
        setcourse(newObj)
        settotal(data.catgoery.total)
        setper_page(data.catgoery.per_page)
      }

}
const getorder=async(e)=>{
 
 let order=e.currentTarget.value
  let token=localStorage.getItem('token');
  let {data}=await axios.get(`http://127.0.0.1:8001/api/getall/`+order,{
      headers: {
      'Accept': 'application/json',
      'Authorization':'Bearer '+token 
      }
    });

    if(data.message==="success"){
    
      var  newObj= [...Object.values(data.catgoery.data)]
      setcourse(newObj)
      settotal(data.catgoery.total)
      setper_page(data.catgoery.per_page)
    }
}
useEffect(() => {
    Allcourse()
  },[]);

    return (
    <div className="container">
        <h1 className="text-center text-white">All Course</h1>
   
        <div className='filters'>
          <h3>Order By:</h3>
        <select className='form-control' onChange={getorder}>
          <option  value="new">new</option>
          <option value="old">old</option>

        </select>

      </div>
        <div className='row '>
      
        {course.map((value)=>(
            <div className="col-md-4 col-sm-12 mt-5 items" key={value.id}>
                <div className="item">
                <img  src={`http://localhost:8001/uploads/catge/${value.img}`}className="img-fluid" alt="..."/>
                <div className='contentall'>
                <p>{value.nameCatgorey}</p>
                <button className='btn'>More Detail</button>
                </div>

                </div>

            </div>
        ))} 
         <div className="pangination">
         <Pagination
          activePage={activePage}
          itemsCountPerPage={per_page}
          totalItemsCount={total}
      
          itemClass="page-item"
          linkClass="page-link"
          onChange={handlePageChange}
        />
         </div>

        </div>

    </div>
  )
}

